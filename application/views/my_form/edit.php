<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">My Form Edit</h3>
            </div>
			<?php echo form_open('my_form/edit/'.$my_form['id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="mail" class="control-label">Mail</label>
						<div class="form-group">
							<input type="text" name="mail" value="<?php echo ($this->input->post('mail') ? $this->input->post('mail') : $my_form['mail']); ?>" class="form-control" id="mail" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="title" class="control-label">Title</label>
						<div class="form-group">
							<input type="text" name="title" value="<?php echo ($this->input->post('title') ? $this->input->post('title') : $my_form['title']); ?>" class="form-control" id="title" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="content" class="control-label">Content</label>
						<div class="form-group">
							<textarea name="content" class="form-control" id="content"><?php echo ($this->input->post('content') ? $this->input->post('content') : $my_form['content']); ?></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>