<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">My Form Add</h3>
            </div>
            <?php echo form_open('my_form/add', ['name'=>'myNameForm', 'id'=>'myIdForm']); ?>
				<div class="box-body">
					<div class="row clearfix">
						<div class="col-md-6">
							<label for="mail" class="control-label">Mail</label>
							<div class="form-group">
								<input type="text" name="mail" value="<?php echo $this->input->post('mail'); ?>" class="form-control" id="mail" />
							</div>
						</div>
						<div class="col-md-6">
							<label for="title" class="control-label">Title</label>
							<div class="form-group">
								<input type="text" name="title" value="<?php echo $this->input->post('title'); ?>" class="form-control" id="title" />
							</div>
						</div>
						<div class="col-md-6">
							<label for="content" class="control-label">Content</label>
							<div class="form-group">
								<textarea name="content" class="form-control" id="content"><?php echo $this->input->post('content'); ?></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<button type="submit" class="btn btn-success">
						<i class="fa fa-check"></i> Save
					</button>
					<button type="button" class="btn btn-success btn-ajax">
						<i class="fa fa-check"></i> Save Ajax
					</button>
				</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>

<script>
	$(() => {
		$('.btn-ajax').click(e =>{
			e.preventDefault();
			const btn = $('.btn-ajax');
			btn.html('Guardando');
			const form = $('#myIdForm');
			const post_url = form.attr("action")+'_ajax';
			const request_method = 'post'; 
			const form_data = form.serialize();
			$.ajax({
				url : post_url,
				type: request_method,
				data : form_data
			});
			}).done(response => { //
				btn.html('Guardado -> ' + response);
		})
	})
</script>